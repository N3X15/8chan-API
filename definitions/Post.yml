title: Post
description: A post as seen in a catalog endpoint.
type: object
required:
- 'no'
- time
- last_modified
- resto
properties:
  'no':
    type: integer
    example: 7613594
    description: Number of this post in each board. NOT unique globally.
  com:
    type: string
    description: HTML content of the post.
    example: <p class=\"body-line ltr \">Ok. This place is out of hand right now.
      I think it's time to crack down.</p><p class=\"body-line empty \"></p><p class=\"body-line
      ltr \">This board being /b/, I'm usually passive on spam, it can stay or it
      can go. That never really mattered too much. However, I think it's absurd to
      have 20+ of the same stupid thread at the same time, they have to go.</p><p
      class=\"body-line empty \"></p><p class=\"body-line ltr \">I am going to shorten
      the pages for a bit and clear up any dupe threads I see, after I'm done I will
      return with more details.</p>
  name:
    type: string
    description: Name of the beast posting.
    example: dysnomia
  trip:
    type: string
    description: Tripcode
    example: '!bO.8VNPLAE'
  capcode:
    type: string
    description: Capcode of the user posting.
    example: Board Volunteer
  time:
    type: integer
    description: unix timestamp of the time the post was made.
    example: 1512933151
  omitted_posts:
    type: integer
    description: Posts hidden on the catalog view.
    example: 1
  omitted_images:
    type: integer
    description: Images hidden on the catalog view.
    example: 1
  replies:
    type: integer
    description: Total replies to the thread.
    example: 2
  images:
    type: integer
    description: Total images in the thread.
    example: 1
  sticky:
    type: integer
    description: 1 if post is stickied, 0 if not.
    example: 1
  locked:
    type: integer
    description: 1 if post is locked, 0 if not.
    example: 1
  cyclical:
    type: string
    description: UNKNOWN! FIX ME!
    example: '0'
  bumplocked:
    type: string
    description: Presumably "1" if the thread can no longer be bumped, "0" if not.
    enum:
    - '1'
    - '0'
    example: '0'
  last_modified:
    type: integer
    description: Unix timestamp of the last change made to the thread.
    example: 1512937329
  id:
    type: string
    description: Hexadecimal ID used to differentiate between posters.  Only on boards
      with `poster_ids` set to `true`.
    example: d0507d
  tn_h:
    type: integer
    description: Thumbnail height, in pixels.
    example: 243
  tn_w:
    type: integer
    description: Thumbnail width, in pixels
    example: 255
  h:
    type: integer
    description: Image height, in pixels
    example: 951
  w:
    type: integer
    description: Image width, in pixels
    example: 1000
  fsize:
    type: integer
    description: Size of the image on disk, in bytes.
    example: 411970
  filename:
    type: string
    description: Original name of the file when it was uploaded.  If unavailable,
      same value as `tim`.
    example: 3c9dbd0fabe1b36d65d5a5abb33a5e68b71bbdb61de45b7e126d833664c2b342
  ext:
    type: string
    description: File extension of the file.
    example: .png
  tim:
    type: string
    description: Long hash (SHA-256?) of the file (in fpath=1) or timestamp of the
      file (fpath=0).
    example: 3c9dbd0fabe1b36d65d5a5abb33a5e68b71bbdb61de45b7e126d833664c2b342
  fpath:
    type: integer
    description: Type of URIs to use when fetching the files for this post. <table>
      <tr> <th>Value</th> <th>Thumbnail/File URIs</th> </tr> <tr> <th rowspan="2">`0`</th>
      <td>`https://media.8ch.net/{BOARD}/thumb/{post.tim}{post.ext}`</td> </tr> <tr>
      <td>`https://media.8ch.net/{BOARD}/src/{post.tim}{post.ext}`</td> </tr> <tr>
      <th rowspan="2">`1` (default)</th> <td>`https://media.8ch.net/file_store/thumb/{post.tim}{post.ext}`</td>
      </tr> <tr> <td>`https://media.8ch.net/file_store/{post.tim}{post.ext}`</td>
      </tr></table>
    example: 1
  spoiler:
    type: integer
    description: Image is hidden due to spoilers if 1, 0 if not.
    example: 1
  md5:
    type: string
    description: Base64-encoded MD5 hash of the file. May have fucked padding.
    example: X9BzLbj8H1CFRJOIAO8uGg==
  resto:
    type: integer
    description: Thread this post replies to, 0 if it IS the thread.
    example: 0
  country:
    type: string
    description: Two-character ISO-639-1 uppercase country code.
    example: MX
  country_name:
    type: string
    description: Capitalized name of the country.
    example: Mexico
